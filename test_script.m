clear;
clc;
close all;

clear apprentice;
workspace = [20, 20];
item_type = [{'A'}, {'B'}, {'C'}, {'D'}];
item_numb = [2 3 4 2];
item_reward = [11 21 22 15];
cost = 2;

% Phase 1: Apprentice learns reward
[learned_policy, rewards] = learning(workspace, item_type,...
    item_numb, item_reward, cost);

% Phase 2: Apprentice Cooperates
[initial_state, initial_exp_start, initial_apprentice_start,...
    initial_expert_plan,expert_actions, apprentice_actions] = ...
    cooperate(learned_policy, cost);

% Plot task
plot_task(initial_state, initial_exp_start, initial_apprentice_start, ...
    initial_expert_plan,expert_actions, apprentice_actions);

% Plot learning
plot_learning(rewards, item_reward)





