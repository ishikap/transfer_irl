function [initial_state, initial_exp_start, initial_apprentice_start,...
    initial_expert_plan,expert_actions, apprentice_actions] = ...
    cooperate(learned_policy, cost)
% This function shows the whole process of apprentice assisting the expert
% doing the task. 

% Note:
% This method doesn't use the optimal global policy. This function assume
% the apprentice has the same policy and capability as the expert, which
% means the apprentice has the same reward and cost policy as the expert

% Inputs:
% learned_policy: 
%         1x(number of types for all the item) matrix, the policy shoud be 
%         the one that learned from observing the expert

% Outputs:
% initial_state: 
%         sum(num_each_symbol)x4 matrix, contains symbol, reward, x, y for 
%         each of the symbols 
%         Note that this is a cell matrix. This is the output of the 
%         generate_state_space function
% initial_exp_start: 
%         1x2 matrix, contains x, y for the start position of the expert
% initial_apprentice_start: 
%         1x2 matrix, contains x,y for the start position of the apprentice
% initial_expert_plan:
%         sum(num_each_symbol)x3 matrix, contains symbol, x, y for each of
%         the symbols. The expert follows this order to pick up items
% expert_actions:
%         ix3 matrix, contains symbol, x, y for items that expert will
%         pick. i is the number of items that expert will pick.
% apprentice_actions:
%         ix3 matrix, contains symbol, x, y for items that apprentice will
%         pick. i is the number of items that apprentice will pick.
%         Note that i+j = sum(num_each_symbol)

state_space = generate_state_space([20 20], [{'A'}, {'B'}, {'C'}, {'D'}],...
    [2 3 4 2], learned_policy);

exp_start = randi([0 20], 1, 2);
apprentice_start = randi([0 20], 1, 2);

expert_actions = [];
apprentice_actions= [];

initial_state = state_space;
initial_exp_start = exp_start;
initial_apprentice_start = apprentice_start;
initial_expert_plan = expert(state_space, exp_start, 0, cost);

while ~isempty(state_space)
    % the apprentice guessed expert move
    expert_plan = expert(state_space, exp_start, 0, cost);
    expert_action = expert_plan(1,:); % APPEND expert_action ={end + 1} = expert_plan(1,:);
    % update the expert position
    exp_start = cell2mat(expert_action(2:3)); % NEED TO CHANGE WHEN APPENDING
    % remove the item that has been picked by the expert
    [index,~] = find(cell2mat(state_space(:,1)) == cell2mat(expert_action(1))...
        & cell2mat(state_space(:,3)) == cell2mat(expert_action(2))...
        & cell2mat(state_space(:,4)) == cell2mat(expert_action(3)) );
    
    state_space(index,:) = [];
    expert_actions = [expert_actions; expert_action];
    
    if isempty(state_space)
        break;
    end
    % apprentice pick the first item on the apprentice's list
    apprentice_plan = expert(state_space, apprentice_start, 0, cost);
    apprentice_action = apprentice_plan(1,:);  %APPEND
    % update the apprentice position
    apprentice_start = cell2mat(apprentice_action(2:3)); % APPEND
    % remove the item picked up by the apprentice
    [index,~] = find(cell2mat(state_space(:,1)) == cell2mat(apprentice_action(1))...
        & cell2mat(state_space(:,3)) == cell2mat(apprentice_action(2))...
        & cell2mat(state_space(:,4)) == cell2mat(apprentice_action(3)) );
    
    state_space(index,:) = [];
    apprentice_actions = [apprentice_actions; apprentice_action];
    
end
end

