function [] = plot_learning(R, item_reward)
figure();
subplot(4,1,1);
hold on;
plot(1:size(R,1), R(:,1), '-r');
plot(1:size(R,1), item_reward(1)*ones(1,size(R,1)), ':r');
title("reward for item A")
grid on;

subplot(4,1,2);
hold on;
plot(1:size(R,1), R(:,2), '-b');
plot(1:size(R,1), item_reward(2)*ones(1,size(R,1)), ':b');
title("reward for item B")
grid on;

subplot(4,1,3);
hold on;
plot(1:size(R,1), R(:,3), '-g');
plot(1:size(R,1), item_reward(3)*ones(1,size(R,1)), ':g');
title("reward for item C")
grid on;

subplot(4,1,4);
hold on;
plot(1:size(R,1), R(:,4), '-k');
plot(1:size(R,1), item_reward(4)*ones(1,size(R,1)), ':k');
grid on;
title("reward for item D")
end

