function [guessed_rewards] = apprentice(expert_actions, expert_start, ...
    rewards_range, cost_range)

% The apprentice tries to estimate the rewards associated with each symbol
% given the expert actions
% This function statically stores the guessed rewards and cost within the
% function and returns the guessed rewards asociated with each symbol and
% the cost

% INPUTS:
% expert_actions: sum(num_each_symbol)x3 matrix, contains symbol, x, y
% expert_start: 1x2 the start coordinates x, y of the expert
% rewards_range: 1x2 matrix range that the reward values can take. the
%                   apprentice is making some assumptions on the possible
%                   rewards
% cost_rane: 1x2 matrix of range that the cost value can take

% OUTPUTS:
% guessed_rewards: num_total_symbols x 2 matrix, contains symbol and
%                   guessed rewards associated with each symbol

% Have a static map from symbol to reward, and a static guess of the cost
persistent symbol_map;
persistent cost_guess;

% If this is the first run of the program, start out the cost guess by
% averageing the range of cost values
if isempty(cost_guess)
    cost_guess = mean(cost_range);
end
% If first run of the program create a map and add the symbol and estimated
% reward of expert action(1, :)
if isempty(symbol_map)
%     symbol_map = containers.Map(char(expert_actions(1,1)),0);
    symbol_map = containers.Map(char(expert_actions(1,1)),mean(rewards_range));

end

% current is where expert starts out and then it keeps getting updated
current = expert_start;

% Iterate through the expert actions and if this is the first time
% observing a symbol, create an initial guess for the reward associated
% with that particular symbol. This initial guess will be the avergae of
% the range for rewards. If this symbol has been observed before,
% update the believed reward for that symbol

for i = 1: (size(expert_actions,1)-1)
    % Ensure that the symbols at i and i+1 of the expert action trail is
    % added to the map
    
    % if this is the first time observing a symbol, add it to the map and
    % initialize the reward associated with it
    if ~isKey(symbol_map, char(expert_actions(i,1)))
%         symbol_map(char(expert_actions(i,1))) = 0;
        symbol_map(char(expert_actions(i,1))) = mean(rewards_range);

    end
    % if the next symbol is not seen, similarly add it to the map and
    % initialize the reward associated with it
    if ~isKey(symbol_map, char(expert_actions(i+1,1)))
%         symbol_map(char(expert_actions(i+1,1))) = 0;
        symbol_map(char(expert_actions(i+1,1))) = mean(rewards_range);
    end
    
    % Now that everything has been added to the map, update the belief of
    % the rewards and cost based on that
    
    % Calculate distance from current to fisrt point and current to second
    % point
    dist_1 = sqrt((current(1) - cell2mat(expert_actions(i,2))).^2 +...
        (current(2)-cell2mat(expert_actions(i,3))).^2);
    dist_2 = sqrt((current(1) - cell2mat(expert_actions(i+1,2))).^2 +...
        (current(2)-cell2mat(expert_actions(i+1,3))).^2);
    % Calculate the guess values of going to first point from current and
    % going to second point from current
    value_1 = symbol_map(char(expert_actions(i,1)))-dist_1*cost_guess;
    value_2 = symbol_map(char(expert_actions(i+1,1)))-dist_2*cost_guess;
    
    if value_1 < value_2
        
        % Only if both the symbols are not the same
        if char(expert_actions(i,1)) ~= char(expert_actions(i+1,1))
            % Increase the first reward if possible
            if symbol_map(char(expert_actions(i,1))) < rewards_range(2)
                symbol_map(char(expert_actions(i,1)))= ...
                    symbol_map(char(expert_actions(i,1))) + ...
                    (mean(rewards_range)/64);
            end
            
            % Decrease the second reward if possible
            if symbol_map(char(expert_actions(i+1,1))) > rewards_range(1)
                symbol_map(char(expert_actions(i+1,1)))= ...
                    symbol_map(char(expert_actions(i+1,1))) - ...
                    (mean(rewards_range)/64);
            end
        end       
        
        
        % Recalculate values 1 and 2 with new cost and reward guesses
        value_1 = symbol_map(char(expert_actions(i,1)))-dist_1*cost_guess;
        value_2 = symbol_map(char(expert_actions(i+1,1)))-dist_2*cost_guess;
        
    end % while value_1 <= value_2
    

end
guessed_rewards = symbol_map;
end

