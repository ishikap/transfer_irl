function [expert_actions] = expert(state_space, start, gamma, cost)

% This function makes the expert use MDP to take actions and returns the
% trail of actiosn which it has followed

% Inputs:
% state_space: sum(num_each_symbol)x4 matrix, contains symbol, reward, x, y
%               for each of the symbols
%               note that this is a cell matrix
%               this is the output of the generate_state_space function
% start: 1x2 matrix with x and y start coordinates of the expert
% gamma: scalar between 0 and 1, future value rate
%           note currently I am making this 0 and myopic
%           including this would make the algorithm very complex
%           so leaving it out for now, will include it later
% cost: scalar cost per meter that it takes the expert

% Outputs:
% expert_actions: sum(num_each_symbol)x3 matrix, contains symbol, x, y
%                   similar to the input, but without the reward
%                   this is being observed by the apprentice 
%                   i.e this output will be passed to the apprentice
%                   function

% Initialize output
expert_actions = [];

% Making a copy of the input to work on just in case
ss = state_space;
current = start;

% Evaluating MDP at each step
% Loop around until all rewards have been collected
% In this case that means until ss is empty
while size(ss,1) ~= 0
    % Evaluate the value of going to each state 
    values = zeros(size(ss,1),1);
    for i = 1:size(values)
        % Value = Reward - distance*cost
        dist = sqrt((current(1) - cell2mat(ss(i,3))).^2 +...
            (current(2)-cell2mat(ss(i,4))).^2);
        values(i) = cell2mat(ss(i,2)) - dist*cost;        
    end
    % Find best location to go to next
    [~, next] = max(values);
    % Go to the best by updating the current
    current(1) = cell2mat(ss(next,3));
    current(2) = cell2mat(ss(next,4));
    % Add the next action to the expert trail
    next_action = ss(next,:);
    next_action(2) = [];
    expert_actions = [expert_actions; next_action];
    % Remove the next action from the current state space
    ss(next, :) = [];
end

end

