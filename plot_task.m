function plot_task(initial_state, initial_exp_start, initial_apprentice_start, ...
    initial_expert_plan, expert_actions, apprentice_actions)

% Import images
% Image of turtlebot which is the expert
turtle = imread('turtlebot.jpg');
% Image of drone as an apprentice
drone = imread('drone.jpg');

% Open a new figure in fullscreen
figure('units','normalized','outerposition',[0 0 1 1])

% The left subplot it just regular expert actions without apprentice
% cooperation
% (setup)
ax1 = subplot(1,2,1);
pbaspect(ax1,[1 1 1])
axis([-2, 22, -2, 22]);
xticks(0:2:20);
yticks(0:2:20);
hold on;
grid on;
title('No Cooperation');

% plot the start position for expert 
turtle_img = image(turtle,...
    'XData',[initial_exp_start(1)-0.5 initial_exp_start(1)+0.5],...
    'YData',[initial_exp_start(2)+0.5 initial_exp_start(2)-0.5]);
turtle_txt = text(initial_exp_start(1)-1.4,initial_exp_start(2)-0.6,'Expert');

% define color, size for different type of items
items = unique(cell2mat(initial_state(:,1)));
colors = {[0.2549    0.6510    0.9569],...
    [0.9569    0.5843    0.2588],...
    [0.9569    0.2745    0.2549],...
    [0.5333    0.9569    0.2549]};
mark_size = [6, 11, 13, 8];
item_imgs = [];
% plot the initial states
for i = 1:size(initial_state,1)
    item_code = find(items == cell2mat(initial_state(i,1)), 1);
    item_img = plot(cell2mat(initial_state(i,3)),...
        cell2mat(initial_state(i,4)),'-mo',...
        'MarkerEdgeColor',cell2mat(colors(item_code)),...
        'MarkerFaceColor',cell2mat(colors(item_code)),...
        'MarkerSize',mark_size(item_code));
    item_imgs = [item_imgs item_img];
end

% Animate the path
for j = 1:size(initial_expert_plan,1)
    
    % First generate trajectory points
    if j == 1
        [x, y] = generate_trajectory(initial_exp_start(1),...
            initial_exp_start(2), cell2mat(initial_expert_plan(1,2)),...
            cell2mat(initial_expert_plan(1,3)));
    else
        [x, y] = generate_trajectory(cell2mat(initial_expert_plan(j-1,2)),...
            cell2mat(initial_expert_plan(j-1,3)),...
            cell2mat(initial_expert_plan(j,2)),...
            cell2mat(initial_expert_plan(j,3)));
    end
    % Then follow those generated trajectory points
    for i = 1:size(x,2)
        % Delete old
        delete(turtle_img);
        delete(turtle_txt);
        % Draw new
        turtle_img = image(turtle,...
            'XData',[x(i)-0.5 x(i)+0.5],...
            'YData',[y(i)+0.5 y(i)-0.5]);
        turtle_txt = text(x(i)-1.4,y(i)-0.6,'Expert');
        drawnow;
        plot(x(i), y(i), '.b');
        %pause(0.2);
    end
    
    % Delete the reward which we already received
    [index,~] = find(cell2mat(initial_state(:,1)) == cell2mat(initial_expert_plan(j,1))...
        & cell2mat(initial_state(:,3)) == cell2mat(initial_expert_plan(j,2))...
        & cell2mat(initial_state(:,4)) == cell2mat(initial_expert_plan(j,3)) );
    delete(item_imgs(index));
    
end

% Animation is done
% Mark the expert start location
plot(initial_exp_start(1), initial_exp_start(2), 'b*');
text(initial_exp_start(1)-1.4,initial_exp_start(2)-0.6,'start');
% Show the states again
% plot the initial states
for i = 1:size(initial_state,1)
    item_code = find(items == cell2mat(initial_state(i,1)), 1);
    item_img = plot(cell2mat(initial_state(i,3)),...
        cell2mat(initial_state(i,4)),'-mo',...
        'MarkerEdgeColor',cell2mat(colors(item_code)),...
        'MarkerFaceColor',cell2mat(colors(item_code)),...
        'MarkerSize',mark_size(item_code));
    item_imgs = [item_imgs item_img];
end
% plot the trajectory for expertice without the assistance from the
% apprentice
plot([initial_exp_start(1); cell2mat(expert_actions(1,2))], ...
    [initial_exp_start(2); cell2mat(expert_actions(1,3))], '-b');
for i = 1:size(initial_expert_plan,1)-1
    plot([cell2mat(initial_expert_plan(i,2)); ...
        cell2mat(initial_expert_plan(i+1,2))], ...
        [cell2mat(initial_expert_plan(i,3)); ...
        cell2mat(initial_expert_plan(i+1,3))], '-b');
end




% (setup)
ax2 = subplot(1,2,2);
pbaspect(ax2,[1 1 1])
axis([-2, 22, -2, 22]);
xticks(0:2:20);
yticks(0:2:20);
hold on;
grid on;
title('With Cooperation');

% plot the start position for expert and the apprentice
turtle_img = image(turtle,...
    'XData',[initial_exp_start(1)-0.5 initial_exp_start(1)+0.5],...
    'YData',[initial_exp_start(2)+0.5 initial_exp_start(2)-0.5]);
drone_img = image(drone,...
    'XData',[initial_apprentice_start(1)-0.5 initial_apprentice_start(1)+0.5],...
    'YData',[initial_apprentice_start(2)+0.5 initial_apprentice_start(2)-0.5]);

turtle_txt = text(initial_exp_start(1)-1.4,initial_exp_start(2)-0.6,'Expert');
drone_txt = text(initial_apprentice_start(1)-2.7,...
    initial_apprentice_start(2)-0.6,'Apprentice');
% define color, size for different type of items
items = unique(cell2mat(initial_state(:,1)));
colors = {[0.2549    0.6510    0.9569],...
    [0.9569    0.5843    0.2588],...
    [0.9569    0.2745    0.2549],...
    [0.5333    0.9569    0.2549]};
mark_size = [6, 11, 13, 8];
item_imgs = [];
% plot the initial states
for i = 1:size(initial_state,1)
    item_code = find(items == cell2mat(initial_state(i,1)), 1);
    item_img = plot(cell2mat(initial_state(i,3)),...
        cell2mat(initial_state(i,4)),'-mo',...
        'MarkerEdgeColor',cell2mat(colors(item_code)),...
        'MarkerFaceColor',cell2mat(colors(item_code)),...
        'MarkerSize',mark_size(item_code));
    item_imgs = [item_imgs item_img];
end



% Animate the path
for j = 1:max(size(expert_actions,1), size(apprentice_actions,1))
    
    % First generate trajectory points
    if j == 1 % meaning very first iteration, start at start
       [exp_x, exp_y] = generate_trajectory(initial_exp_start(1),...
            initial_exp_start(2), cell2mat(expert_actions(1,2)),...
            cell2mat(expert_actions(1,3)));
        [app_x, app_y] = generate_trajectory(initial_apprentice_start(1),...
            initial_apprentice_start(2), cell2mat(apprentice_actions(1,2)),...
            cell2mat(apprentice_actions(1,3)));
    else
        if j <= size(expert_actions,1)
            [exp_x, exp_y] = generate_trajectory(cell2mat(expert_actions(j-1,2)),...
                cell2mat(expert_actions(j-1,3)),...
                cell2mat(expert_actions(j,2)),...
                cell2mat(expert_actions(j,3)));
        end
        if j <= size(apprentice_actions,1)
            [app_x, app_y] = generate_trajectory(cell2mat(apprentice_actions(j-1,2)),...
                cell2mat(apprentice_actions(j-1,3)),...
                cell2mat(apprentice_actions(j,2)),...
                cell2mat(apprentice_actions(j,3)));
        end
    end
    
    % Expert and Apprentice both follow their respective trajectory points
    for i = 1:max(size(app_x,2), size(exp_x,2))
        % For turtle: If it hasn't reached yet
        if i <= size(exp_x,2) && j <= size(expert_actions,1)
            % Delete old turtle
            delete(turtle_img);
            delete(turtle_txt);
            % Draw new
            turtle_img = image(turtle,...
                'XData',[exp_x(i)-0.5 exp_x(i)+0.5],...
                'YData',[exp_y(i)+0.5 exp_y(i)-0.5]);
            turtle_txt = text(exp_x(i)-1.4,exp_y(i)-0.6,'Expert');
            plot(exp_x(i), exp_y(i), '.b');
        end
        % Similarly, if drone hasn't reached yet
        if i <= size(app_x,2) && j <= size(apprentice_actions,1)
            % Delete old turtle
            delete(drone_img);
            delete(drone_txt);
            % Draw new
            drone_img = image(drone,...
                'XData',[app_x(i)-0.5 app_x(i)+0.5],...
                'YData',[app_y(i)+0.5 app_y(i)-0.5]);
            drone_txt = text(app_x(i)-1.4,app_y(i)-0.6,'Apprentice');
            plot(app_x(i), app_y(i), '.r');
        end
        drawnow;
        pause(0.2);      
    end
    
    % If expert collected a reward, delete it
    if j <= size(expert_actions,1)
        [index,~] = find(cell2mat(initial_state(:,1)) == cell2mat(expert_actions(j,1))...
            & cell2mat(initial_state(:,3)) == cell2mat(expert_actions(j,2))...
            & cell2mat(initial_state(:,4)) == cell2mat(expert_actions(j,3)) );
        delete(item_imgs(index));
    end
    % Similarly if apprentice collected a reward, delete it
    if j <= size(apprentice_actions,1)
        [index,~] = find(cell2mat(initial_state(:,1)) == cell2mat(apprentice_actions(j,1))...
            & cell2mat(initial_state(:,3)) == cell2mat(apprentice_actions(j,2))...
            & cell2mat(initial_state(:,4)) == cell2mat(apprentice_actions(j,3)) );
        delete(item_imgs(index));
    end
end

% Animation completed
% Mark the expert start location
plot(initial_exp_start(1), initial_exp_start(2), 'b*');
text(initial_exp_start(1)-1.4,initial_exp_start(2)-0.6,'expert start');
% Mark apprentice start location
plot(initial_apprentice_start(1), initial_apprentice_start(2), 'r*');
text(initial_apprentice_start(1)-1.4,initial_apprentice_start(2)-0.6,'apprentice start');
% Place the rewards back
for i = 1:size(initial_state,1)
    item_code = find(items == cell2mat(initial_state(i,1)), 1);
    item_img = plot(cell2mat(initial_state(i,3)),...
        cell2mat(initial_state(i,4)),'-mo',...
        'MarkerEdgeColor',cell2mat(colors(item_code)),...
        'MarkerFaceColor',cell2mat(colors(item_code)),...
        'MarkerSize',mark_size(item_code));
    item_imgs = [item_imgs item_img];
end

% plot the trajectory for both the expert and the apprentice
plot([initial_exp_start(1); cell2mat(expert_actions(1,2))], ...
    [initial_exp_start(2); cell2mat(expert_actions(1,3))], '-b');
plot([initial_apprentice_start(1); cell2mat(apprentice_actions(1,2))], ...
    [initial_apprentice_start(2); cell2mat(apprentice_actions(1,3))], '-r');
for i = 1:size(expert_actions, 1)-1
    plot([cell2mat(expert_actions(i,2)); ...
        cell2mat(expert_actions(i+1,2))], ...
        [cell2mat(expert_actions(i,3)); ...
        cell2mat(expert_actions(i+1,3))], '-b');
end
for i = 1:size(apprentice_actions)-1
    plot([cell2mat(apprentice_actions(i,2)); ...
        cell2mat(apprentice_actions(i+1,2))], ...
        [cell2mat(apprentice_actions(i,3)); ...
        cell2mat(apprentice_actions(i+1,3))], '-r');
end


end
