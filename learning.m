function [learned_policy, rewards] = learning(workspace, item_type, item_numb, item_reward,cost)
%% apprentice learning policy
R = [];
for j = 1:1500
    exp_start = randi([0 20], 1, 2);
    state_space = generate_state_space(workspace, item_type, ...
        item_numb, item_reward);
    expert_actions = expert(state_space, exp_start, 0, cost);
    [guessed_rewards] = apprentice(expert_actions, exp_start, [10 25], [0 1]);
    R = [R; cell2mat(guessed_rewards.values)];
    if mod(j,1000) == 0
        disp(j);
    end
end

learned_policy = cell2mat(guessed_rewards.values);
rewards = R;
end

