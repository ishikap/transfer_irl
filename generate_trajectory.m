function [x, y] = generate_trajectory(start_x,start_y,end_x, end_y)

x = [];
y = [];

kv = 0.5;
dt = 0.5;

current_x = start_x;
current_y = start_y;

error =  sqrt(((end_y-current_y).^2) + ((end_x-current_x) .^2));
theta = atan2((end_y-current_y),(end_x-current_x));

while error > 0.1
    x = [x current_x];
    y = [y current_y];
    v = kv*error;
    current_x = current_x + v*cos(theta)*dt;
    current_y = current_y + v*sin(theta)*dt;
    error =  sqrt(((end_y-current_y).^2) + ((end_x-current_x) .^2));
    theta = atan2((end_y-current_y),(end_x-current_x));
end


end

