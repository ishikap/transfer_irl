function [state_space] = generate_state_space(size_of_space, ...
    symbols, num_each_symbol, rewards)

% This function places the rewards and symbols at random locations in the
% given space

% INPUTS:
% size_of_space: 1x2 matrix with x and y dimensions of the state space
%                   note that this needs to be an array of cells of char
% symbols: 1xN matrix with of characters/symbols
% num_each_symbol: 1xN matrix of number of each symbol in this space
% rewards: 1xN matrix of rewards associated with each symbol
% N is the total number of different symbols

% OUTPUTS:
% state_space: sum(num_each_symbol)x4 matrix, contains symbol, reward, x, y
%               for each of the symbols
%               note that this is a cell matrix

% generate randon x and y coordinates
x = randi([0 size_of_space(1)], sum(num_each_symbol), 1);
y = randi([0 size_of_space(2)], sum(num_each_symbol), 1);

% create symbols, rewards associated with each x and y coordinate
temp_sym = [];
temp_rew = [];
for i = 1:length(symbols)
    temp_sym = [temp_sym; repmat(symbols(i), num_each_symbol(i), 1)];
    temp_rew = [temp_rew; repmat(rewards(i), num_each_symbol(i), 1)];
end

% concatenate everything for output
state_space = [temp_sym, num2cell(temp_rew), num2cell(x), num2cell(y)];

end

